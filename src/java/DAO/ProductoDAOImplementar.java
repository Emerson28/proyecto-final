package DAO;

import Factory.ConexionDB;
import Factory.FactoryConexionDB;
import Model.Producto;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;



public class ProductoDAOImplementar implements ProductoDAO{

    ConexionDB conn;
    
   
    public ProductoDAOImplementar() {
       
    }        
    
    @Override
    public List<Producto> Listar() {
        this.conn = FactoryConexionDB.open(FactoryConexionDB.MySQL);
        StringBuilder miSQL = new StringBuilder();
        miSQL.append("SELECT * FROM tb_producto;");
        List<Producto> Lista = new ArrayList<>();
        try{
            ResultSet ResultadoSQL = this.conn.consultaSQL(miSQL.toString());
            while(ResultadoSQL.next()){
                Producto producto = new Producto();
                producto.setId_producto(ResultadoSQL.getInt("id_producto"));
                producto.setNom_producto(ResultadoSQL.getString("nom_producto"));
                producto.setStock(ResultadoSQL.getFloat("stock"));
                producto.setPrecio(ResultadoSQL.getFloat("precio"));
                producto.setUnidadMedida(ResultadoSQL.getString("unidad_de_medida"));
                producto.setEstado(ResultadoSQL.getInt("estado_producto"));
                producto.setCategoria_id(ResultadoSQL.getInt("categoria"));
                Lista.add(producto);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductoDAOImplementar.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            this.conn.cerrarConexion();
        }
        
        return Lista;
    }

    @Override
    public List<Producto> Listar2() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Producto editarProd(int id_product_edit) {
        this.conn = FactoryConexionDB.open(FactoryConexionDB.MySQL);
        Producto producto = new Producto();        
        StringBuilder miSQL = new StringBuilder();
        
        miSQL.append("SELECT * FROM tb_producto WHERE id_producto =").append(id_product_edit);
        try{
            ResultSet resultadoSQL = this.conn.consultaSQL(miSQL.toString());
            while(resultadoSQL.next()){
                                               
                producto.setId_producto(resultadoSQL.getInt("id_producto"));
                producto.setNom_producto(resultadoSQL.getString("nom_producto"));
                producto.setCategoria_id(resultadoSQL.getInt("categoria_id"));
                producto.setStock(resultadoSQL.getFloat("stock"));
                producto.setPrecio(resultadoSQL.getFloat("precio"));
                producto.setUnidadMedida(resultadoSQL.getString("unidadMedida"));
                producto.setEstado(resultadoSQL.getInt("estado"));
             
                
            }
        }catch(Exception e){
            
        }finally{
            this.conn.cerrarConexion();
        }
        return producto;
    }

    @Override
    public boolean guardarProd(Producto producto) {
        this.conn = FactoryConexionDB.open(FactoryConexionDB.MySQL);
        boolean guarda = false;
        
        try{
            if(producto.getId_producto() == 0){
                StringBuilder miSQL = new StringBuilder();
                miSQL.append("INSERT INTO tb_producto (nom_producto, categoria_id, stock, precio, unidad_de_medida, estado) VALUES('");
                miSQL.append(producto.getNom_producto()+ "', ").append(producto.getCategoria_id()+ "', ").append(producto. getStock()).append(producto.getPrecio()+ "', ").append(producto.getUnidadMedida()+ "', ").append(producto.getEstado());
                miSQL.append(");");
                this.conn.ejecutarSQL(miSQL.toString());
                
            }else if(producto. getId_producto()> 0){
                StringBuilder miSQL = new StringBuilder();
                miSQL.append("UPDATE tb_producto SET id_producto = ").append(producto.getId_producto());
                miSQL.append(", nom_producto = '").append(producto.getNom_producto());
                miSQL.append("', categoria_id = ").append(producto.getCategoria_id());
                
                miSQL.append("', stock = ").append(producto.getStock());
                miSQL.append("', precio = ").append(producto.getPrecio());
                miSQL.append("', unidadMedida = ").append(producto.getUnidadMedida());
                miSQL.append("', estado = ").append(producto.getEstado());
                //
                
                miSQL.append(" WHERE id_producto = ").append(producto.getId_producto()).append(";");
                this.conn.ejecutarSQL(miSQL.toString());
            }
            guarda = true;
        }catch(Exception e){
            this.conn.cerrarConexion();
        }
        return guarda; 
    }

    @Override
    public boolean borrarProd(int id_product_borrar) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }    
}

