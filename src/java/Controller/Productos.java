
package Controller;

import DAO.ProductoDAO;
import DAO.ProductoDAOImplementar;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Productos extends HttpServlet {

    protected void listarProductos(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        response.setContentType("text/html;charset=UTF-8");
        ProductoDAO producto = new ProductoDAOImplementar();
        HttpSession sesion = request.getSession(true);
        sesion.setAttribute("lista", producto.Listar());
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/Vistas-Productos/listarProductos.jsp");
        dispatcher.forward(request, response);
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        }
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String parametro = request.getParameter("opcion");
        
        if(parametro.equals("crear")){
            
            String pagina = "/Vistas-Productos/crearProducto.jsp";
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(pagina);
            dispatcher.forward(request, response);
            
        }else if(parametro.equals("listar")){
            
            this.listarProductos(request, response);
            
        }else if(parametro.equals("modificar")){
            
            int id_producto = Integer.parseInt(request.getParameter("id_product"));
            String nom_producto = request.getParameter("nom_product");
            int cat_producto = Integer.parseInt(request.getParameter("cat_product"));
            float stock_producto = Float.valueOf(request.getParameter("stock_product"));
            float precio_producto = Float.valueOf(request.getParameter("precio_product"));
            String unidadMedida_producto = request.getParameter("unidadMedida_product");            
            int estado_producto = Integer.parseInt(request.getParameter("estado_product"));            
            
            String pagina = "/Vistas-Categorias/crearProducto.jsp?id_p="+id_producto+"&&nombre_p="+nom_producto+"&&catego_p="+cat_producto+"&&stock_p="+stock_producto+"&&precio_p="+precio_producto+"&&unidadMedida_p="+unidadMedida_producto+"&&estado_p="+estado_producto+"&&senal=1";
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(pagina);
            dispatcher.forward(request, response);
        }else if(parametro.equals("eliminar")){
            
        }
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}

