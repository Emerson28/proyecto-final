<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="categoria" scope="session" class="Model.Categoria"/>

<% 
    String id = "";
    String nombre_cat = "";
    String estado_cat = "";
    
    if(request.getParameter("senal") != null){
        /*String id = request.getParameter("id_c");        
        String nombre_cat = request.getParameter("nombre_c");
        String estado_cat = request.getParameter ("estado_c");
        */
        id = request.getParameter("id_c");
        nombre_cat = request.getParameter("nombre_c");
        estado_cat = request.getParameter("estado_c");
    }else{
        id = String.valueOf(categoria.getId_categoria());
        nombre_cat = categoria.getNom_categoria();
        estado_cat = String.valueOf(categoria.getEstado_categoria());
    }
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/estilos.css">
        <title>Control de Inventario</title>
        <%@include file = "../WEB-INF/Vistas-Parciales/css-jsp.jspf" %>
        <script type="text/javascript">
            function regresar(url) {
                location.href = url;
            }
        </script>

    </head>
<body>
        <%@include file= "../WEB-INF/Vistas-Parciales/encabezado.jspf" %>

        <div class="caja-crear bg-dark mx-auto shadow p-3 mb-5 rounded-3 border-5">
            <h1 class="fs-1 text-light position-relative" >Mantenimiento Categorias</h1>

            <form class="form-horizontal" id="frmCategoria" name="frmCategoria" action="<%= request.getContextPath()%>/categorias" method="post">
                <input type="hidden" name="id_categoria" value="<%= id %>">
                <label class="nomest form-label"><strong> NOMBRE:</strong></label><input type="text" class="input-estadonombre form-control" name="txtNomCategoria" value="<%=  nombre_cat %>"><br><br>
                <label class="nomest form-label"><strong> ESTADO:</strong></label><input type="text" class="input-estadonombre form-control" name="txtEstadoCategoria" value="<%= categoria.getEstado_categoria()%>"><br><br>

                <br><br>
                <center>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <%
                              if(request.getParameter("senal") != null){                              
                            %>
                            <input type="submit" class="btn btn-danger btn-sm" name="btnModificar" value="Actualizar"/>
                            <%                                                                   
                                }else{
                            %>                            
                            <input type="submit" class="btn btn-success btn-sm" name="btnGuardar" value="Guardar" />
                            <%
                            }
                            %>
                            <input type="button" class="btn btn-danger btn-sm" onclick="regresar('<%= request.getContextPath()%>/categorias?opcion=listar')" name="btnRegresar" value="Regresar" />
                        </div>
                    </div>
                </center>
           </form>
        </div>
    <%@include file= "../WEB-INF/Vistas-Parciales/pie.jspf" %>
</body>
</html>
