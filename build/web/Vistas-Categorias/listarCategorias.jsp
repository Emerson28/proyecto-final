<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page import = "Model.Categoria" %>
<jsp:useBean id = "lista" scope = "session" class = "java.util.List" />
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/estilos.css">
        <link href="SA/sweetalert2.css" rel="stylesheet" type="text/css"/>
        <title>Control de inventario</title>
    <%@include file= "../WEB-INF/Vistas-Parciales/css-jsp.jspf"  %>
         </head>
    <body>
        <%@include file= "../WEB-INF/Vistas-Parciales/encabezado.jspf" %>
        <!--<div class="container-mostrar mx-auto bg-warning border-dark shadow p-3 mb-5 rounded-3">
            
            <h1 class="text-dark" align="center">Listado de Categorias Registradas</h1>
        </div>-->
        <div class="container-mostrar mx-auto bg-info border-dark shadow p-3 mb-5 rounded-3">
            <a href="<%= request.getContextPath() %>/categorias?opcion=crear" class="btn btn-success btn-sm glyphicon glyphicon-pencil" role="button"> Nueva Categoria</a>
             <h1 class="text-dark" align="center">Listado de Categorias Registradas</h1>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th class="text-dark border-dark border-2 bg-success" scope="col">ID</th>
                        <th class="text-dark border-dark border-2 bg-success" scope="col">NOMBRE</th>
                        <th class="text-dark border-dark border-2 bg-success" scope="col">ESTADO</th>
                        <th class="text-dark border-dark border-2 bg-success" scope="col">ACCION</th>
                    </tr>
                </thead>
                
                <%
                    for(int i=0 ; i < lista.size(); i++){
                        Categoria categoria = new Categoria();
                        categoria = (Categoria)lista.get(i);
                %>
                <tr>
                    <td><%= categoria.getId_categoria() %></td>
                    <td><%= categoria.getNom_categoria() %></td>
                    <td><%= categoria.getEstado_categoria() %></td>
                    <td>
                        <a href="<%= request.getContextPath()%>/categorias?opcion=modificar&&id_cat=<%= categoria.getId_categoria()%>&&nombre_cat=<%= categoria.getNom_categoria()%>&&estado_cat=<%= categoria.getEstado_categoria()%>"
                           
                           class="btn btn-warning btn-sm glyphicon glyphicon-edit" role="button" name="btnmodi" data-toggle="modal" data_target = "#largeModal">Editar</a>
                        
                           <!--Eliminar-->
                        <a href="<%= request.getContextPath()%>/categorias?opcion=eliminar&&id_cat=<%= categoria.getId_categoria()%>" class="btn btn-danger btn-sm glyphicon glyphicon-remove" role="button" name="btneli">Eliminar</a>                        
                    </td>
                </tr>
                <%
                    }
                %>
                
            </table>
        </div>
        
        <%@include file= "../WEB-INF/Vistas-Parciales/pie.jspf" %>
        <script src="SA/sweetalert2.js" type="text/javascript"></script>
        <script src="JS/jquery.min.js" type="text/javascript"></script>
        <script src="JS/funcionesUser.js" type="text/javascript"></script>
    </body>
</html>
